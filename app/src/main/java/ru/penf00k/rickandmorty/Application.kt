package ru.penf00k.rickandmorty

import android.app.Application
import ru.penf00k.rickandmorty.di.AppComponent
import ru.penf00k.rickandmorty.di.ContextModule
import ru.penf00k.rickandmorty.di.DaggerAppComponent

class Application : Application() {

    lateinit var appComponent: AppComponent
        private set

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .contextModule(ContextModule(this))
            .build()
    }
}