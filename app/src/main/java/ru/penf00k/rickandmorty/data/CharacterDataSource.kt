package ru.penf00k.rickandmorty.data

import io.reactivex.Single
import ru.penf00k.rickandmorty.data.gateway.retrofit.CharactersWithNextPage
import ru.penf00k.rickandmorty.model.character.Character


interface CharacterRepository {
    fun getCharacters(charactersPerPage: Int, page: Int): Single<List<Character>>
    fun getCharacter(id: Int): Single<Character>
}

interface CharacterStorage {
    fun getCharacters(charactersPerPage: Int, page: Int): Single<List<Character>>
    fun setCharacters(characters: List<Character>)
    fun getCharacter(id: Int): Single<Character>
}

interface CharacterGateway {
    fun getCharacters(page: Int): Single<CharactersWithNextPage>
}