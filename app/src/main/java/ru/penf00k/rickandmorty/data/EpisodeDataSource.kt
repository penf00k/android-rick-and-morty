package ru.penf00k.rickandmorty.data

import io.reactivex.Single
import ru.penf00k.rickandmorty.model.episode.Episode


interface EpisodeRepository {
    fun getEpisodes(characterId: Int): Single<List<Episode>>
}

interface EpisodeStorage {
    fun getEpisodes(ids: List<Int>): Single<List<Episode>>
    fun setEpisodes(episodes: List<Episode>)
}

interface EpisodeGateway {
    fun getEpisodes(ids: List<Int>): Single<List<Episode>>
}