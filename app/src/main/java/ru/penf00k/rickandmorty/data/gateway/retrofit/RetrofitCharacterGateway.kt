package ru.penf00k.rickandmorty.data.gateway.retrofit

import io.reactivex.Single
import ru.penf00k.rickandmorty.data.CharacterGateway
import ru.penf00k.rickandmorty.model.character.Character

data class CharactersWithNextPage(val characters: List<Character>, val nextPage: String)

class RetrofitCharacterGateway(private val apiService: RickAndMortyApiService) : CharacterGateway {

    override fun getCharacters(page: Int): Single<CharactersWithNextPage> {
        return apiService.getCharacters(page)
            .map { response -> response.toCharactersWithNextPage() }
    }
}