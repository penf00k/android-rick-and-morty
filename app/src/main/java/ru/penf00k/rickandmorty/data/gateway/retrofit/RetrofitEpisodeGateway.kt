package ru.penf00k.rickandmorty.data.gateway.retrofit

import io.reactivex.Single
import ru.penf00k.rickandmorty.data.EpisodeGateway
import ru.penf00k.rickandmorty.model.episode.Episode

class RetrofitEpisodeGateway(private val apiService: RickAndMortyApiService) : EpisodeGateway {

    override fun getEpisodes(ids: List<Int>): Single<List<Episode>> {
        when (ids.size) {
            0 -> return Single.just(listOf())
            1 -> return apiService.getEpisode(ids[0])
                .map { response ->
                    listOf(response.toEpisodeModel())
                }
            else -> {
                val idsString = StringBuilder()
                idsString.append(ids[0])
                for (i in 1 until ids.size) {
                    idsString
                        .append(",")
                        .append(ids[i])
                }

                return apiService.getEpisodes(idsString.toString())
                    .map { response ->
                        val episodes = mutableListOf<Episode>()
                        response.forEach { e -> episodes.add(e.toEpisodeModel()) }
                        episodes
                    }
            }
        }
    }
}