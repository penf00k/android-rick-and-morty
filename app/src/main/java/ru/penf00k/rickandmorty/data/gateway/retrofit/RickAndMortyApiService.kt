package ru.penf00k.rickandmorty.data.gateway.retrofit

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import ru.penf00k.rickandmorty.data.gateway.retrofit.dto.character.CharacterResponse
import ru.penf00k.rickandmorty.data.gateway.retrofit.dto.episode.EpisodeResponse

interface RickAndMortyApiService {

    @GET("/api/character/")
    fun getCharacters(@Query("page") page: Int): Single<CharacterResponse>

    @GET("/api/episode/{id}")
    fun getEpisode(@Path("id") id: Int): Single<EpisodeResponse>

    @GET("/api/episode/{ids}")
    fun getEpisodes(@Path("ids") ids: String): Single<List<EpisodeResponse>>
}