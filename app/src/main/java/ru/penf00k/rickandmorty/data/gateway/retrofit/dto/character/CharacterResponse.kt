package ru.penf00k.rickandmorty.data.gateway.retrofit.dto.character

import ru.penf00k.rickandmorty.data.gateway.retrofit.CharactersWithNextPage
import ru.penf00k.rickandmorty.model.character.Character

data class CharacterResponse(
    val info: Info,
    val results: List<Result>
) {
    private fun toCharacterList(): List<Character> {
        val result = mutableListOf<Character>()
        results.forEach { c -> result.add(c.toCharacter()) }
        return result
    }

    fun toCharactersWithNextPage() =
        CharactersWithNextPage(toCharacterList(), info.next)
}