package ru.penf00k.rickandmorty.data.gateway.retrofit.dto.character

data class Info(
    val count: Int,
    val next: String,
    val pages: Int,
    val prev: String
)