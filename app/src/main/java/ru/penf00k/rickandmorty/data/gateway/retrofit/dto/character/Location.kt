package ru.penf00k.rickandmorty.data.gateway.retrofit.dto.character

data class Location(
    val name: String,
    val url: String
)