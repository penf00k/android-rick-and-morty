package ru.penf00k.rickandmorty.data.gateway.retrofit.dto.character

data class Origin(
    val name: String,
    val url: String
)