package ru.penf00k.rickandmorty.data.gateway.retrofit.dto.character

import ru.penf00k.rickandmorty.model.character.Character
import ru.penf00k.rickandmorty.model.character.Episodes

data class Result(
    val created: String,
    val episode: List<String>,
    val gender: String,
    val id: Int,
    val image: String,
    val location: Location,
    val name: String,
    val origin: Origin,
    val species: String,
    val status: String,
    val type: String,
    val url: String
) {
    fun toCharacter(): Character {
        return Character(
            created,
            Episodes(
                episode
            ),
            gender,
            id,
            image,
            location.name,
            name,
            origin.name,
            species,
            status,
            type,
            url
        )
    }
}