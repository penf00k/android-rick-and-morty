package ru.penf00k.rickandmorty.data.gateway.retrofit.dto.episode

import ru.penf00k.rickandmorty.model.episode.Episode

data class EpisodeResponse(
    val air_date: String,
    val characters: List<String>,
    val created: String,
    val episode: String,
    val id: Int,
    val name: String,
    val url: String
) {
    fun toEpisodeModel() = Episode(
        episode,
        id,
        name
    )
}