package ru.penf00k.rickandmorty.data.repository

import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import ru.penf00k.rickandmorty.data.CharacterGateway
import ru.penf00k.rickandmorty.data.CharacterRepository
import ru.penf00k.rickandmorty.data.CharacterStorage
import ru.penf00k.rickandmorty.data.gateway.retrofit.CharactersWithNextPage
import ru.penf00k.rickandmorty.model.character.Character

class CharacterRepo(
    private val storage: CharacterStorage,
    private val gateway: CharacterGateway
) : CharacterRepository {

    override fun getCharacters(charactersPerPage: Int, page: Int): Single<List<Character>> {
        return storage.getCharacters(charactersPerPage, page)
            .subscribeOn(Schedulers.io())
            .flatMap { characters ->
                if (characters.isEmpty())
                    gateway.getCharacters(page)
                        .onErrorReturn { e ->
                            if (e is HttpException && e.code() == 404) {
                                return@onErrorReturn CharactersWithNextPage(listOf(), "")
                            }

                            throw e
                        }
                        .doOnSuccess { charactersWithNextPage ->
                            storage.setCharacters(charactersWithNextPage.characters)
                        }.map { charactersWithNextPage -> charactersWithNextPage.characters }
                else {
                    Single.just(characters)
                }
            }
    }

    override fun getCharacter(id: Int): Single<Character> =
        storage.getCharacter(id)
            .subscribeOn(Schedulers.io())
}