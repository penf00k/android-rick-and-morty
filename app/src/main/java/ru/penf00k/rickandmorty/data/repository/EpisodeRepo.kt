package ru.penf00k.rickandmorty.data.repository

import io.reactivex.Single
import ru.penf00k.rickandmorty.data.CharacterRepository
import ru.penf00k.rickandmorty.data.EpisodeGateway
import ru.penf00k.rickandmorty.data.EpisodeRepository
import ru.penf00k.rickandmorty.data.EpisodeStorage
import ru.penf00k.rickandmorty.model.episode.Episode
import ru.penf00k.rickandmorty.util.contains

class EpisodeRepo(
    private val storage: EpisodeStorage,
    private val gateway: EpisodeGateway,
    private val characterRepo: CharacterRepository
) : EpisodeRepository {

    override fun getEpisodes(characterId: Int): Single<List<Episode>> {
        return characterRepo.getCharacter(characterId)
            .flatMap { character ->
                val ids = character.episode.toEpisodeIds()

                return@flatMap storage.getEpisodes(ids)
                    .map { storedEpisodes -> Pair(ids, storedEpisodes) }
            }.flatMap { pair ->
                val ids = pair.first
                val storedEpisodes = pair.second

                val episodesToFetch = mutableListOf<Int>()

                ids.forEach { id ->
                    if (!storedEpisodes.contains(id) { id, episode: Episode -> episode.id == id })
                        episodesToFetch.add(id)
                }

                if (episodesToFetch.size > 0) {
                    return@flatMap gateway.getEpisodes(episodesToFetch)
                        .doOnSuccess { e -> storage.setEpisodes(e) }
                        .map { ids }
                }

                return@flatMap Single.just(ids)
            }.flatMap { ids ->
                return@flatMap storage.getEpisodes(ids)
            }
    }
}