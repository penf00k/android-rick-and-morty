package ru.penf00k.rickandmorty.data.storage

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ru.penf00k.rickandmorty.data.storage.character.CharacterDao
import ru.penf00k.rickandmorty.data.storage.episode.EpisodeDao
import ru.penf00k.rickandmorty.model.character.Character
import ru.penf00k.rickandmorty.model.episode.Episode

@Database(entities = [Character::class, Episode::class], version = 1)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun characterDao(): CharacterDao
    abstract fun episodeDao(): EpisodeDao
}