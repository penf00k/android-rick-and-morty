package ru.penf00k.rickandmorty.data.storage

import androidx.room.TypeConverter
import ru.penf00k.rickandmorty.model.character.Episodes

class Converters {

    companion object {
        private const val STRING_SEPARATOR = "œ∑´´†¥"
    }

    @TypeConverter
    fun toEpisodes(input: String?): Episodes? {
        return Episodes(
            input?.split(STRING_SEPARATOR)
                ?: listOf()
        )
    }

    @TypeConverter
    fun fromEpisodes(input: Episodes?): String? {
        input?.episodes?.let { episodes ->
            val sb = StringBuilder()
            sb.append(episodes[0])
            for (i in 1 until episodes.size) {
                sb.append(STRING_SEPARATOR).append(episodes[i])
            }
            return sb.toString()
        }
    }
}