package ru.penf00k.rickandmorty.data.storage.character

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Single
import ru.penf00k.rickandmorty.model.character.Character

@Dao
interface CharacterDao {

    @Query("SELECT * FROM characters WHERE id = (:id)")
    fun getCharacter(id: Int): Single<Character>

    @Query("SELECT * FROM characters WHERE id BETWEEN (:start) AND (:end)")
    fun getCharacters(start: Int, end: Int): Single<List<Character>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun setCharacters(characters: List<Character>)
}