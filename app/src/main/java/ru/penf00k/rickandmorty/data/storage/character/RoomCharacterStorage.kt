package ru.penf00k.rickandmorty.data.storage.character

import io.reactivex.Single
import ru.penf00k.rickandmorty.data.CharacterStorage
import ru.penf00k.rickandmorty.data.storage.AppDatabase
import ru.penf00k.rickandmorty.model.character.Character

class RoomCharacterStorage(private val db: AppDatabase) : CharacterStorage {

    override fun getCharacters(charactersPerPage: Int, page: Int): Single<List<Character>> {
        val start = (page - 1) * charactersPerPage
        return db.characterDao().getCharacters(start + 1, start + charactersPerPage)
    }

    override fun setCharacters(characters: List<Character>) {
        db.characterDao().setCharacters(characters)
    }

    override fun getCharacter(id: Int): Single<Character> {
        return db.characterDao().getCharacter(id)
    }
}