package ru.penf00k.rickandmorty.data.storage.episode

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Single
import ru.penf00k.rickandmorty.model.episode.Episode

@Dao
interface EpisodeDao {

    @Query("SELECT * FROM episodes WHERE id IN (:ids)")
    fun getEpisodes(ids: List<Int>): Single<List<Episode>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun setEpisodes(episodes: List<Episode>)
}