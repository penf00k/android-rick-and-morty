package ru.penf00k.rickandmorty.data.storage.episode

import io.reactivex.Single
import ru.penf00k.rickandmorty.data.EpisodeStorage
import ru.penf00k.rickandmorty.data.storage.AppDatabase
import ru.penf00k.rickandmorty.model.episode.Episode

class RoomEpisodeStorage(private val db: AppDatabase) : EpisodeStorage {

    override fun getEpisodes(ids: List<Int>): Single<List<Episode>> {
        return db.episodeDao().getEpisodes(ids)
    }

    override fun setEpisodes(episodes: List<Episode>) {
        return db.episodeDao().setEpisodes(episodes)
    }
}