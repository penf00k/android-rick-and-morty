package ru.penf00k.rickandmorty.di

import dagger.Component
import ru.penf00k.rickandmorty.presenter.CharacterDetailPresenter
import ru.penf00k.rickandmorty.presenter.CharacterListPresenter
import javax.inject.Singleton

@Singleton
@Component(modules = [ContextModule::class, RepositoryModule::class])
interface AppComponent {
    fun inject(target: CharacterListPresenter)
    fun inject(target: CharacterDetailPresenter)
}