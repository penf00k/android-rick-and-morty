package ru.penf00k.rickandmorty.di

import dagger.Module
import dagger.Provides
import ru.penf00k.rickandmorty.data.CharacterGateway
import ru.penf00k.rickandmorty.data.EpisodeGateway
import ru.penf00k.rickandmorty.data.gateway.retrofit.RetrofitCharacterGateway
import ru.penf00k.rickandmorty.data.gateway.retrofit.RetrofitEpisodeGateway
import ru.penf00k.rickandmorty.data.gateway.retrofit.RickAndMortyApiService
import javax.inject.Singleton

@Module(includes = [RickAndMortyApiServiceModule::class])
class GatewayModule {

    @Provides
    @Singleton
    fun provideCharacterGateway(apiService: RickAndMortyApiService): CharacterGateway =
        RetrofitCharacterGateway(apiService)

    @Provides
    @Singleton
    fun provideEpisodeGateway(apiService: RickAndMortyApiService): EpisodeGateway =
        RetrofitEpisodeGateway(apiService)
}