package ru.penf00k.rickandmorty.di

import dagger.Module
import dagger.Provides
import ru.penf00k.rickandmorty.data.*
import ru.penf00k.rickandmorty.data.repository.CharacterRepo
import ru.penf00k.rickandmorty.data.repository.EpisodeRepo
import javax.inject.Singleton

@Module(includes = [StorageModule::class, GatewayModule::class])
class RepositoryModule {

    @Provides
    @Singleton
    fun provideCharacterRepository(storage: CharacterStorage, gateway: CharacterGateway): CharacterRepository =
        CharacterRepo(storage, gateway)

    @Provides
    @Singleton
    fun provideEpisodeRepository(storage: EpisodeStorage, gateway: EpisodeGateway, characterRepo: CharacterRepository): EpisodeRepository =
        EpisodeRepo(storage, gateway, characterRepo)
}