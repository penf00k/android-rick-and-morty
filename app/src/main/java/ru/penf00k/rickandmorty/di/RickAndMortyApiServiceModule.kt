package ru.penf00k.rickandmorty.di

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import ru.penf00k.rickandmorty.data.gateway.retrofit.RickAndMortyApiService
import javax.inject.Singleton

@Module(includes = [RetrofitModule::class])
class RickAndMortyApiServiceModule {

    @Provides
    @Singleton
    fun provideRickAndMortyApiService(retrofit: Retrofit): RickAndMortyApiService =
        retrofit.create(RickAndMortyApiService::class.java)
}