package ru.penf00k.rickandmorty.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import ru.penf00k.rickandmorty.data.storage.AppDatabase
import javax.inject.Singleton

@Module(includes = [ContextModule::class])
class RoomDatabaseModule {

    @Provides
    @Singleton
    fun provideRoomDatabase(context: Context): AppDatabase = Room.databaseBuilder(
        context,
        AppDatabase::class.java, "database-name"
    ).build()
}