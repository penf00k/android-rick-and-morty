package ru.penf00k.rickandmorty.di

import dagger.Module
import dagger.Provides
import ru.penf00k.rickandmorty.data.CharacterStorage
import ru.penf00k.rickandmorty.data.EpisodeStorage
import ru.penf00k.rickandmorty.data.storage.AppDatabase
import ru.penf00k.rickandmorty.data.storage.character.RoomCharacterStorage
import ru.penf00k.rickandmorty.data.storage.episode.RoomEpisodeStorage
import javax.inject.Singleton

@Module(includes = [RoomDatabaseModule::class])
class StorageModule {

    @Provides
    @Singleton
    fun provideCharacterStorage(db: AppDatabase): CharacterStorage =
        RoomCharacterStorage(db)

    @Provides
    @Singleton
    fun provideEpisodeStorage(db: AppDatabase): EpisodeStorage =
        RoomEpisodeStorage(db)
}