package ru.penf00k.rickandmorty.model.character

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "characters")
data class Character(
    @PrimaryKey val created: String,
    val episode: Episodes,
    val gender: String,
    val id: Int,
    val image: String,
    val location: String,
    val name: String,
    val origin: String,
    val species: String,
    val status: String,
    val type: String,
    val url: String
) {
    fun toCharacterInfoItems(): List<CharacterInfoItem> = listOf(
        CharacterInfoItem("Species", species),
        CharacterInfoItem("Gender", gender),
        CharacterInfoItem("Status", status),
        CharacterInfoItem("Origin", origin),
        CharacterInfoItem("Location", location)
    )
}

data class CharacterInfoItem(
    val name: String,
    val value: String
)

data class Episodes(
    val episodes: List<String>
) {
    private fun getEpisodeId(url: String) =
        url.trim().split("/").last().toInt()

    fun toEpisodeIds(): List<Int> {
        val result = mutableListOf<Int>()
        episodes.forEach { e -> result.add(getEpisodeId(e)) }
        return result
    }
}
