package ru.penf00k.rickandmorty.model.episode

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "episodes")
data class Episode(
//    val air_date: String,
//    val characters: List<String>,
//    val created: String,
    val episode: String,
    @PrimaryKey val id: Int,
//    val url: String,
    val name: String
)