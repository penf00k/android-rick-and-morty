package ru.penf00k.rickandmorty.presenter

import android.content.Context
import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import ru.penf00k.rickandmorty.Application
import ru.penf00k.rickandmorty.data.CharacterRepository
import ru.penf00k.rickandmorty.data.EpisodeRepository
import ru.penf00k.rickandmorty.view.character.detail.CharacterDetailView
import javax.inject.Inject

@InjectViewState
class CharacterDetailPresenter(context: Context) : MvpPresenter<CharacterDetailView>() {

    @Inject
    protected lateinit var characterRepo: CharacterRepository

    @Inject
    protected lateinit var episodeRepo: EpisodeRepository

    init {
        (context as Application).appComponent.inject(this)
    }

    private var getCharacterSubscription: Disposable? = null

    fun getCharacter(id: Int) {
        getCharacterSubscription?.dispose()
        getCharacterSubscription = characterRepo.getCharacter(id)
            .flatMap { character ->
                return@flatMap episodeRepo.getEpisodes(character.id)
                    .map { episodes -> Pair(character, episodes) }
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { pair ->
                    val character = pair.first
                    val episodes = pair.second
                    viewState.showCharacter(character, episodes)
                },
                { error -> Log.d("TAGTAG", "Error = $error") }
            )
    }

    override fun onDestroy() {
        super.onDestroy()
        getCharacterSubscription?.dispose()
    }
}