package ru.penf00k.rickandmorty.presenter

import android.content.Context
import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import ru.penf00k.rickandmorty.Application
import ru.penf00k.rickandmorty.data.CharacterRepository
import ru.penf00k.rickandmorty.view.character.list.CharacterListView
import javax.inject.Inject

@InjectViewState
class CharacterListPresenter(context: Context) : MvpPresenter<CharacterListView>() {

    @Inject
    protected lateinit var characterRepo: CharacterRepository

    init {
        (context as Application).appComponent.inject(this)
    }

    private var getNextCharactersSubscription: Disposable? = null
    private var charactersPerPage = 20
    private var listSize = 0

    fun getNextCharacters() {
        viewState.showProgress()
        getNextCharactersSubscription?.dispose()
        val page = Math.ceil(listSize.toDouble() / charactersPerPage).toInt()
        getNextCharactersSubscription = characterRepo.getCharacters(charactersPerPage, page + 1)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { characters ->
                    viewState.hideProgress()
                    if (characters.isEmpty())
                        return@subscribe

                    listSize += characters.size
                    viewState.showMoreCharacters(characters)
                },
                { error ->
                    Log.e("TAGTAG", "Error = $error")
                    viewState.setError(error)
                }
            )
    }

    override fun onDestroy() {
        super.onDestroy()
        getNextCharactersSubscription?.dispose()
    }
}