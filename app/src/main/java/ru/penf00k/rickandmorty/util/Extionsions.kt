package ru.penf00k.rickandmorty.util


fun <T, V> List<T>.contains(candidate: V, test: (candidate: V, value: T) -> Boolean): Boolean {
    forEach { item ->
        if (test(candidate, item))
            return true
    }

    return false
}