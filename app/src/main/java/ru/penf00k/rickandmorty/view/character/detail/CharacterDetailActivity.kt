package ru.penf00k.rickandmorty.view.character.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.activity_character_detail.*
import ru.penf00k.rickandmorty.R
import ru.penf00k.rickandmorty.model.character.Character
import ru.penf00k.rickandmorty.model.episode.Episode
import ru.penf00k.rickandmorty.presenter.CharacterDetailPresenter


class CharacterDetailActivity : MvpAppCompatActivity(), CharacterDetailView {

    companion object {
        private const val EXTRA_CHARACTER_ID = "ru.penf00k.rickandmorty.view.character.detail.character_id"

        fun newIntent(packageContext: Context, id: Int) =
            Intent(packageContext, CharacterDetailActivity::class.java)
                .apply {
                    putExtra(EXTRA_CHARACTER_ID, id)
                }
    }

    @InjectPresenter
    internal lateinit var presenter: CharacterDetailPresenter

    @ProvidePresenter
    internal fun provideCharacterDetailPresenter(): CharacterDetailPresenter {
        return CharacterDetailPresenter(applicationContext)
    }

    private lateinit var adapter: EpisodeAdapter

    override fun showCharacter(character: Character, episodes: List<Episode>) {
        setTitle(character.name)
        adapter.setData(character, episodes)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_character_detail)

        adapter = EpisodeAdapter()

        rv_episodes.layoutManager = LinearLayoutManager(this)
        rv_episodes.adapter = adapter

        val id = intent.getIntExtra(EXTRA_CHARACTER_ID, 1)

        presenter.getCharacter(id)
    }
}
