package ru.penf00k.rickandmorty.view.character.detail

import com.arellomobile.mvp.MvpView
import ru.penf00k.rickandmorty.model.character.Character
import ru.penf00k.rickandmorty.model.episode.Episode

interface CharacterDetailView : MvpView {
    fun showCharacter(character: Character, episodes: List<Episode>)
}