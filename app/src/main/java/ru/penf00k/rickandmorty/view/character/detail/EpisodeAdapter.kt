package ru.penf00k.rickandmorty.view.character.detail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_list_character_details_episode.view.*
import kotlinx.android.synthetic.main.item_list_character_details_image.view.*
import kotlinx.android.synthetic.main.item_list_character_details_header.view.*
import ru.penf00k.rickandmorty.GlideApp
import ru.penf00k.rickandmorty.R
import ru.penf00k.rickandmorty.model.character.Character
import ru.penf00k.rickandmorty.model.character.CharacterInfoItem
import ru.penf00k.rickandmorty.model.episode.Episode
import ru.penf00k.rickandmorty.util.createCircularProgressDrawable

class EpisodeAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val VIEW_TYPE_IMAGE = 0
        private const val VIEW_TYPE_HEADER = 1
        private const val VIEW_TYPE_INFO = 2
        private const val VIEW_TYPE_EPISODE = 3
    }

    private var character: Character? = null
    private val characterInfoItems = mutableListOf<CharacterInfoItem>()
    private val episodes = mutableListOf<Episode>()
    private val headers = listOf("Character data", "Episodes")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_IMAGE ->
                ImageViewHolder(
                    LayoutInflater
                        .from(parent.context)
                        .inflate(R.layout.item_list_character_details_image, parent, false)
                )
            VIEW_TYPE_HEADER ->
                HeaderViewHolder(
                    LayoutInflater
                        .from(parent.context)
                        .inflate(R.layout.item_list_character_details_header, parent, false)
                )
            VIEW_TYPE_INFO ->
                InfoViewHolder(
                    LayoutInflater
                        .from(parent.context)
                        .inflate(android.R.layout.simple_list_item_1, parent, false)
                )
            else -> EpisodeViewHolder(
                LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.item_list_character_details_episode, parent, false)
            )
        }
    }

    override fun getItemCount(): Int {
        return 1 + headers.size + characterInfoItems.size + episodes.size
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> VIEW_TYPE_IMAGE
            1, characterInfoItems.size + 2 -> VIEW_TYPE_HEADER
            in 2..characterInfoItems.size + 1 -> VIEW_TYPE_INFO
            else -> VIEW_TYPE_EPISODE
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            VIEW_TYPE_IMAGE ->
                (viewHolder as ImageViewHolder).bind(character)
            VIEW_TYPE_HEADER ->
                (viewHolder as HeaderViewHolder).bind(getHeader(position))
            VIEW_TYPE_INFO ->
                (viewHolder as InfoViewHolder).bind(getInfoViewHolder(position))
            VIEW_TYPE_EPISODE ->
                (viewHolder as EpisodeViewHolder).bind(getEpisode(position))
        }
    }

    private fun getHeader(position: Int) =
        headers[if (position == 1) 0 else 1]

    private fun getInfoViewHolder(position: Int) =
        characterInfoItems[position - 2]

    private fun getEpisode(position: Int) =
        episodes[position - characterInfoItems.size - headers.size - 1]

    fun setData(character: Character, episodes: List<Episode>) {
        this.character = character
        characterInfoItems.addAll(character.toCharacterInfoItems())
        this.episodes.addAll(episodes)
        notifyDataSetChanged()
    }
}

class ImageViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

    fun bind(character: Character?) {
        val context = view.context

        view.tv_character_name.text = character?.name

        GlideApp.with(context)
            .load(character?.image)
            .centerCrop()
            .placeholder(createCircularProgressDrawable(context))
            .error(R.drawable.ic_broken_image_black_24dp)
            .into(view.iv_character_detail_image)
    }
}

class InfoViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

    fun bind(info: CharacterInfoItem) {
        view.findViewById<TextView>(android.R.id.text1).text = "${info.name}: ${info.value}"
    }
}

class EpisodeViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

    fun bind(episode: Episode) {
        view.tv_episode_name.text = "${episode.episode}: ${episode.name}"
    }
}

class HeaderViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

    fun bind(text: String) {
        view.tv_header.text = text
    }
}