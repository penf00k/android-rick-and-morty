package ru.penf00k.rickandmorty.view.character.list

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import kotlinx.android.synthetic.main.item_list_character.view.*
import kotlinx.android.synthetic.main.item_list_loading.view.*
import ru.penf00k.rickandmorty.GlideApp
import ru.penf00k.rickandmorty.R
import ru.penf00k.rickandmorty.model.character.Character
import ru.penf00k.rickandmorty.util.createCircularProgressDrawable
import ru.penf00k.rickandmorty.view.character.detail.CharacterDetailActivity

typealias OnRetry = () -> Unit

class CharacterAdapter(
    private val characters: MutableList<Character>,
    private val onRetry: OnRetry
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val VIEW_TYPE_CHARACTER = 0
        private const val VIEW_TYPE_LOADING = 1
    }

    var isLoading = false
        set(value) {
            field = value
            if (value)
                notifyItemInserted(characters.size + 1)
            else
                notifyItemRemoved(characters.size + 1)
        }

    private var hasError = false

    private var loadingViewHolder: LoadingViewHolder? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val context = parent.context

        when (viewType) {
            VIEW_TYPE_CHARACTER -> {
                return CharacterViewHolder(
                    LayoutInflater
                        .from(context)
                        .inflate(R.layout.item_list_character, parent, false),
                    createCircularProgressDrawable(context)
                )
            }
            else -> {
                loadingViewHolder = LoadingViewHolder(
                    LayoutInflater
                        .from(context)
                        .inflate(R.layout.item_list_loading, parent, false),
                    onRetry
                )
                return loadingViewHolder!!
            }
        }
    }

    private fun shouldShowFooter() = isLoading || hasError

    override fun getItemCount(): Int {
        return if (shouldShowFooter()) characters.size + 1 else characters.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (shouldShowFooter() && position == characters.size)
            VIEW_TYPE_LOADING
        else
            VIEW_TYPE_CHARACTER
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            VIEW_TYPE_CHARACTER ->
                (viewHolder as CharacterViewHolder).bind(characters[position])
            VIEW_TYPE_LOADING ->
                (viewHolder as LoadingViewHolder).bind(isLoading, hasError)
        }
    }

    fun addItems(items: List<Character>) {
        hasError = false
        if (items.isEmpty())
            return

        val positionStart = characters.size
        characters.addAll(items)
        notifyItemRangeInserted(positionStart, items.size)
    }

    fun setError(error: Throwable) {
        hasError = true
        loadingViewHolder?.setError(error)
    }
}

class CharacterViewHolder(
    private val view: View,
    private val circularProgressDrawable: CircularProgressDrawable
) : RecyclerView.ViewHolder(view) {

    fun bind(character: Character) {
        val context = view.context

        view.tv_primary.text = character.name
        view.tv_secondary.text = character.species

        GlideApp.with(context)
            .load(character.image)
            .circleCrop()
            .placeholder(circularProgressDrawable)
            .error(R.drawable.ic_broken_image_black_24dp)
            .into(view.iv_character_image)

        itemView.setOnClickListener {
            Log.d("CharacterAdapter", "Character id is ${character.id}")
            val intent = CharacterDetailActivity.newIntent(context, character.id)
            context.startActivity(intent)
        }
    }
}

class LoadingViewHolder(
    private val view: View,
    private val onRetry: OnRetry
) : RecyclerView.ViewHolder(view) {

    init {
        view.btn_retry.setOnClickListener { onRetry() }
    }

    fun bind(isLoading: Boolean, hasError: Boolean) {
        if (hasError) {
            setErrorView()
        } else if (isLoading) {
            view.iv_status.setImageDrawable(createCircularProgressDrawable(itemView.context))
            view.btn_retry.visibility = GONE
        }
    }

    fun setError(error: Throwable) {
        //TODO set error message
        setErrorView()
    }

    private fun setErrorView() {
        view.iv_status.setImageResource(R.drawable.ic_error_outline_black_24dp)
        view.btn_retry.visibility = VISIBLE
    }
}