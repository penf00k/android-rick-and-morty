package ru.penf00k.rickandmorty.view.character.list

import com.arellomobile.mvp.MvpView
import ru.penf00k.rickandmorty.model.character.Character

interface CharacterListView : MvpView {
    fun showMoreCharacters(characters: List<Character>)
    fun showProgress()
    fun hideProgress()
    fun setError(error: Throwable)
}