package ru.penf00k.rickandmorty.view.character.list

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.activity_characters_list.*
import ru.penf00k.rickandmorty.R
import ru.penf00k.rickandmorty.model.character.Character
import ru.penf00k.rickandmorty.presenter.CharacterListPresenter


class CharactersListActivity : MvpAppCompatActivity(), CharacterListView {

    @InjectPresenter
    internal lateinit var presenter: CharacterListPresenter

    @ProvidePresenter
    internal fun provideCharacterListPresenter(): CharacterListPresenter {
        return CharacterListPresenter(applicationContext)
    }

    override fun showMoreCharacters(characters: List<Character>) {
        adapter.addItems(characters)
    }

    override fun showProgress() {
        adapter.isLoading = true
    }

    override fun hideProgress() {
        adapter.isLoading = false
    }

    override fun setError(error: Throwable) {
        adapter.setError(error)
    }

    private val characters = mutableListOf<Character>()
    private lateinit var adapter: CharacterAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_characters_list)

        adapter = CharacterAdapter(characters) { loadNextCharacters() }

        rv_characters.layoutManager = LinearLayoutManager(this)
        rv_characters.adapter = adapter
        setRecyclerViewScrollListener(rv_characters)
        loadNextCharacters()
    }

    private fun setRecyclerViewScrollListener(rv: RecyclerView) {
        rv.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                (recyclerView.layoutManager as? LinearLayoutManager)?.let { llm ->
                    val totalItemCount = llm.itemCount
                    val lastVisibleItemPosition = llm.findLastVisibleItemPosition()

                    if (!(recyclerView.adapter as CharacterAdapter).isLoading && totalItemCount == lastVisibleItemPosition + 1)
                        loadNextCharacters()
                }
            }
        })
    }

    private fun loadNextCharacters() {
        presenter.getNextCharacters()
    }
}
